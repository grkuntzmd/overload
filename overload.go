package main

import (
	"fmt"
	"log"
	"strings"
)

type Car struct {
	make  *string
	model *string
	year  *int
	color *string
}

func NewCar(options ...option) (*Car, error) {
	c := &Car{}
	for _, o := range options {
		err := o(c)
		if err != nil {
			return nil, err
		}
	}

	return c, nil
}

func PrintCar(c *Car) string {
	s := []string{}
	if c.make != nil {
		s = append(s, fmt.Sprintf("Make %s", *c.make))
	}
	if c.model != nil {
		s = append(s, fmt.Sprintf("Model %s", *c.model))
	}
	if c.year != nil {
		s = append(s, fmt.Sprintf("Year %d", *c.year))
	}
	if c.color != nil {
		s = append(s, fmt.Sprintf("Color %s", *c.color))
	}

	return fmt.Sprintf("Car { %s }", strings.Join(s, ", "))
}

type option func(*Car) error

func WithMake(make string) option {
	return func(c *Car) error {
		c.make = &make
		return nil
	}
}

func WithModel(model string) option {
	return func(c *Car) error {
		c.model = &model
		return nil
	}
}

func WithYear(year int) option {
	return func(c *Car) error {
		if year < 1900 || year > 2017 {
			return fmt.Errorf("Year must be in the range 1900 to 2017. Found %d", year)
		}

		c.year = &year
		return nil
	}
}

func WithColor(color string) option {
	return func(c *Car) error {
		c.color = &color
		return nil
	}
}

func main() {
	c, err := NewCar(WithMake("Ford"), WithModel("Mustang"), WithYear(1965))
	if err != nil {
		log.Panicf("Error building car: %s", err)
	}
	fmt.Printf("Car: %v\n", PrintCar(c))

	c, err = NewCar(WithMake("Chevrolet"), WithModel("Camaro"), WithYear(2020), WithColor("red"))
	if err != nil {
		log.Panicf("Error building car: %s", err)
	}
	fmt.Printf("Car: %v\n", PrintCar(c))
}
